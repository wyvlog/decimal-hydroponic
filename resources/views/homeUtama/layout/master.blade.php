<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
   <title>SMARTPONE - Smart Ponik Education</title>
  <!-- Favicon -->
  <link rel="icon" href="{{URL::asset('utama/img/core-img/logoS.png')}}">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="{{URL::asset('utama/style.css')}}">
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  @include ('homeUtama.include.header')

  @yield('breadcrump')

  <!-- ##### Hero Area End ##### -->

  <!-- ##### Famie Benefits Area Start ##### -->
  @yield('content')
  <!-- ##### Farming Practice Area End ##### -->

  <!-- ##### Footer Area Start ##### -->
  @include('homeUtama.include.footer')
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="{{URL::asset('utama/js/jquery.min.js')}}"></script>
  <!-- Popper js -->
  <script src="{{URL::asset('utama/js/popper.min.js')}}"></script>
  <!-- Bootstrap js -->
  <script src="{{URL::asset('utama/js/bootstrap.min.js')}}"></script>
  <!-- Owl Carousel js -->
  <script src="{{URL::asset('utama/js/owl.carousel.min.js')}}"></script>
  <!-- Classynav -->
  <script src="{{URL::asset('utama/js/classynav.js')}}"></script>
  <!-- Wow js -->
  <script src="{{URL::asset('utama/js/wow.min.js')}}"></script>
  <!-- Sticky js -->
  <script src="{{URL::asset('utama/js/jquery.sticky.js')}}"></script>
  <!-- Magnific Popup js -->
  <script src="{{URL::asset('utama/js/jquery.magnific-popup.min.js')}}"></script>
  <!-- Scrollup js -->
  <script src="{{URL::asset('utama/js/jquery.scrollup.min.js')}}"></script>
  <!-- Jarallax js -->
  <script src="{{URL::asset('utama/js/jarallax.min.js')}}"></script>
  <!-- Jarallax Video js -->
  <script src="{{URL::asset('utama/js/jarallax-video.min.js')}}"></script>
  <!-- Active js -->
  <script src="{{URL::asset('utama/js/active.js')}}"></script>
</body>

</html>