@extends('homeUtama.layout.master')
@section('breadcrump')
      <!-- Single Welcome Slides -->
  <div class="hero-area">
    <div class="welcome-slides owl-carousel">
      <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/gambar1.jpg');">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 col-lg-10">
              <div class="welcome-content">
                <h2 data-animation="fadeInDown" data-delay="200ms"> Smart Ponik Education (SMARTPONE)</h2>
                <p data-animation="fadeInDown" data-delay="400ms">SMARTPONE Merupakan Suatu Aplikasi yang bertujuan untuk memberiakn informasi seputar Tanaman Hidroponik berupa keperluan untuk menanam keperluan Tanaman Hidroponik berupa keperluan untuk menanam, jadwal perawatan dan mendapatkan iformasi penyakit tanaman Hidroponik dengan metode penalaran</p>
                <a href="#" class="btn famie-btn mt-4" data-animation="bounceInUp" data-delay="600ms">Read More</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Single Welcome Slides -->
      <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/gambar2.jpg');">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 col-lg-10">
              <div class="welcome-content">
                <h2 data-animation="fadeInDown" data-delay="200ms"> Smart Ponik Education (SMARTPONE)</h2>
                <p data-animation="fadeInDown" data-delay="400ms">SMARTPONE Merupakan Suatu Aplikasi yang bertujuan untuk memberikan informasi seputar Tanaman Hidroponik berupa keperluan untuk menanam keperluan Tanaman Hidroponik berupa keperluan untuk menanam, jadwal perawatan dan mendapatkan iformasi penyakit tanaman Hidroponik dengan metode penalaran</p>
                <a href="#" class="btn famie-btn mt-4" data-animation="bounceInDown" data-delay="600ms">Read More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('content')
      <section class="famie-benefits-area section-padding-100-0 pb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="benefits-thumbnail mb-50">
            <img src="{{URL::asset('utama/img/bg-img/gambar3.jpg')}}" alt="">
          </div>
        </div>
      </div>

      <div class="row justify-content-center">
        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="100ms">
            <img src="{{URL::asset('utama/img/core-img/digger.png')}}" alt="">
            <h5>Rekomendasi</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="300ms">
            <img src="{{URL::asset('utama/img/core-img/windmill.png')}}" alt="">
            <h5>Keperluan Menanam</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="500ms">
            <img src="{{URL::asset('utama/img/core-img/cereals.png')}}" alt="">
            <h5>Cara Penanaman dan Penjadwalan</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="700ms">
            <img src="{{URL::asset('utama/img/core-img/tractor.png')}}" alt="">
            <h5>Video Tentang Tanaman Hidroponik</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="900ms">
            <img src="{{URL::asset('utama/img/core-img/cari.png')}}" alt="">
            <h5>Konsultasi Penyakit Tanaman</h5>
          </div>
        </div>
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="900ms">
            <img src="{{URL::asset('utama/img/core-img/diskusi.png')}}" alt="">
            <h5>Forum Diskusi</h5>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Famie Benefits Area End ##### -->

  <!-- ##### About Us Area Start ##### -->
  <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Content -->
        <div class="col-12 col-md-8">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <p>Video Hidroponik</p>
              <h2><span>Let Us</span> Hasil Pertanian Hidroponik</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            <p>Hidroponik adalah budidaya menanam dengan memanfaatkan air tanpa menggunakan tanah dengan menekankan pada pemenuhan kebutuhan nutrisi bagi tanaman. Kebutuhan air pada hidroponik lebih sedikit daripada kebutuhan air pada budidaya dengan tanah.</p>
            <a href="{{url('video')}}" class="btn famie-btn mt-30">Read More</a>
          </div>
        </div>

        <!-- Famie Video Play -->
        <div class="col-12 col-md-4">
          <div class="famie-video-play mb-100">
            <img src="{{URL::asset('utama/img/bg-img/6.jpg')}}" alt="">
            <!-- Play Icon -->
            <a href="http://www.youtube.com/watch?v=7HKoqNJtMTQ" class="play-icon"><i class="fa fa-play"></i></a>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- ##### About Us Area End ##### -->

  <!-- ##### Services Area Start ##### -->
  <section class="services-area d-flex flex-wrap">
    <!-- Service Thumbnail -->
    <div class="services-thumbnail bg-img jarallax" style="background-image: url('utama/img/bg-img/gambar2.jpg');"></div>

    <!-- Service Content -->
    <div class="services-content section-padding-100-50 px-5">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Keperluan </span> untuk menanam tanaman Hidroponik</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 mb-50">
            <p>Pada fitur ini, pengguna akan diberikan list berupa keperluan keperluan untuk menanam tanaman hidroponik yang telah dipilih sebelumnya.</p>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="100ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="{{URL::asset('utama/img/core-img/benih.png')}}" alt="">
                <h5>Benih</h5>
              </div>
              <p>selain netpot dan wadah untuk menempatkan tanaman, dalam metode hidroponik sedrhana dibutuhkan wadah untuk menampung air dan nutrisi yang terlarut. Bak atau ember plastik dapat digunakan untuk keperluan tersebut. Pilihlah bak atau wadah plastik yang tidak bocor dan jangan lupa untuk membersihkannya sebelum digunakan. </p>
            </div>
          </div>
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="100ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="{{URL::asset('utama/img/core-img/sun.png')}}" alt="">
                <h5>Cahaya Matahari</h5>
              </div>
              <p>Seperti pada umumnya tanaman yang hidup dikebun atau media tanah, tanaman hidroponik memerlukan penerangan 8–10 jam cahaya matahari untuk setiap harinya, supaya menghasilkan pertumbuhan tanaman yang sempurna. Dapat juga memakai lampu yang berkemampuan baik sejenis lampu LED bercahaya terang yang dapat dipakai jika tidak ada cahaya matahari.</p>
            </div>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="300ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="{{URL::asset('utama/img/core-img/pipa.png')}}" alt="">
                <h5>Pipa Paralon</h5>
              </div>
              <p>Tidak hanya bak atau wadah plastik saja yang dapat digunakan untuk menampung air atau nutrisi yang diperlukan tanaman dalam metode hidroponok. Pipa paralon bekas dengan ukuran yang cukup besar dapat digunakan untuk menampung air dan nutrisi hidroponik. </p>
            </div>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="500ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="{{URL::asset('utama/img/core-img/pot.png')}}" alt="">
                <h5>Netpot</h5>
              </div>
              <p>Netpot adalah istilah untuk wadah atau pot tanaman yang biasanya berukuran kecil dan berlubang. Netpot merupakan salah satu peralatan hidroponik sederhana yang sebenarnya dapat kita buat sendiri di rumah. Untuk mengganti netpot kita dapat menggunakan gelas plastik bekas air mineral atau gelas plastik yang sudah tak terpakai lagi dan kemudian dilubangi seperti netpot.  Penggunaan netpot dari gelas plastik beas air mineral lebih ekonomis dan tentunya ramah lingkungan.</p>
            </div>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="700ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="{{URL::asset('utama/img/core-img/ph-meter.png')}}" alt="">
                <h5>PH Meter</h5>
              </div>
              <p>kadar keasaman larutan nutrisi dan media tanam perlu dijaga untuk memastikan pertumbuhan tanaman yang optimal. Untuk mengetahui kadar pH media tanam atau nutrisi digunakanlah pH meter.</p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!-- ##### Services Area End ##### -->

  <!-- ##### Our Products Area Start ##### -->
  <section class="our-products-area section-padding-100">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- Section Heading -->
          <div class="section-heading text-center">
            <h2><span>Rekomendasi</span> Tanaman Hidroponik</h2>
            <img src="{{URL::asset('utama/img/core-img/decor2.png')}}" alt="">
          </div>
        </div>
      </div>

      <div class="row">

        <!-- Single Product Area -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="100ms">
            <!-- Product Thumbnail -->
            <div class="product-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar4.jpg')}}" alt="">
              <!-- Product Tags -->
              <!-- Product Meta Data -->
              <div class="product-meta-data">
                <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="icon_heart_alt"></i></a>
            
              </div>
            </div>
            <!-- Product Description -->
            <div class="product-desc text-center pt-4">
              <a href="#" class="product-title">Strawberry</a>
            </div>
          </div>
        </div>

        <!-- Single Product Area -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="300ms">
            <!-- Product Thumbnail -->
            <div class="product-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar5.jpg')}}" alt="">
              <!-- Product Meta Data -->
              <div class="product-meta-data">
                <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="icon_heart_alt"></i></a>
              </div>
            </div>
            <!-- Product Description -->
            <div class="product-desc text-center pt-4">
              <a href="#" class="product-title">Sayur Hijau</a>
        
            </div>
          </div>
        </div>

        <!-- Single Product Area -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="500ms">
            <!-- Product Thumbnail -->
            <div class="product-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar6.jpg')}}" alt="">
              <!-- Product Meta Data -->
              <div class="product-meta-data">
                <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="icon_heart_alt"></i></a>
                
              </div>
            </div>
            <!-- Product Description -->
            <div class="product-desc text-center pt-4">
              <a href="#" class="product-title">Tomat</a>
            </div>
          </div>
        </div>

        <!-- Single Product Area -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="700ms">
            <!-- Product Thumbnail -->
            <div class="product-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar7.jpg')}}" alt="">
              <!-- Product Tags -->
            
              <!-- Product Meta Data -->
              <div class="product-meta-data">
                <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="icon_heart_alt"></i></a>
                
              </div>
            </div>
            <!-- Product Description -->
            <div class="product-desc text-center pt-4">
              <a href="#" class="product-title">Cabai</a>
              
            </div>
          </div>
        </div>

      </div>

      <div class="row">
        <div class="col-12">
          <div class="gotoshop-btn text-center wow fadeInUp" data-wow-delay="900ms">
            <a href="{{url('rekomendasi')}}" class="btn famie-btn">Read More</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Our Products Area End ##### -->

  <!-- ##### Newsletter Area Start ##### -->
  <section class="newsletter-area section-padding-100 bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/8.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10">
          <div class="newsletter-content">
            <!-- Section Heading -->
            <div class="section-heading white text-center">
              <p>What we do</p>
              <h2><span>Konsultasi </span> Penyakit Tanaman</h2>
              <img src="{{URL::asset('utama/img/core-img/decor2.png')}}" alt="">
            </div>
            <h4 class="text-white mb-50 text-center">Pada fitur ini, pengguna dapat berkonsultasi tentang kondisi Tanaman Hidroponik yang ditanam, dan sistem akan memberikan solusi langsung menggunakan metode Penalaran.</h4>
          </div>
        </div>
      </div>
      <!-- Newsletter Form -->
      <div class="row justify-content-center">
        <div class="col-12 col-lg-3">
            <a href="{{route('konsultasi')}}" class="btn famie-btn">Mulai Konsultasi Tanaman</a>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Newsletter Area End ##### -->

  <!-- ##### Farming Practice Area Start ##### -->
  <section class="farming-practice-area section-padding-100-50">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- Section Heading -->
          <div class="section-heading text-center">
            <h2><span>Cara Penanaman </span> dan Penjadwalan</h2>
            <img src="{{URL::asset('utama/img/core-img/decor2.png')}}" alt="">
          </div>
        </div>
      </div>

      <div class="row center">

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="400ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/12.jpg')}}" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="{{URL::asset('utama/img/core-img/cereal.png')}}" alt="">
              </div>
              <span>Cara Penanaman</span>
              <h4>Cara Penanaman tumbuhan Hidroponik</h4>
            </div>
          </div>
        </div>

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="500ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/13.jpg')}}" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="{{URL::asset('utama/img/core-img/sprout.png')}}" alt="">
              </div>
              <span>Penjadwalan</span>
              <h4>sistem Memberikan Informasi Penjadwalan merawat tanaman</h4>
            </div>
          </div>
        </div>

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="600ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/14.jpg')}}" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="{{URL::asset('utama/img/core-img/vegetable.png')}}" alt="">
              </div>
              <span>Hasil</span>
              <h4>Hasil Dari Melakukan Penanaman dan perawatan sesuai penjadwalan</h4>
            </div>            
          </div>          
        </div>        
      </div>
      <center><a href="{{url('penjadwalan')}}" class="btn famie-btn">Read More</a></center>
    </div>
  </section>
@endsection
@section('script')

@endsection
