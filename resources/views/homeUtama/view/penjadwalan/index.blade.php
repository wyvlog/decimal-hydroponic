@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/gambar3.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Penjadwalan</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Penjadwalan</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Content -->
        <div class="col-12 col-lg-12">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Penjadwalan Tanaman</span> Hydroponic</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            
            <center><h2>Pilih Tanaman</h2></center>
            <div class="row">
            @for($i=4;$i<=7;$i++)
              <!-- Single Product Area -->
              <div class="col-12 col-sm-6 col-lg-3" id="result_{{$i}}" style="box-shadow: 0 8px 4px 0 rgba(0,0,0,0.2);">
                <div class="single-product-area mb-50 fadeInUp">
                  <!-- Product Thumbnail -->
                  <div class="product-thumbnail">
                    <img src="{{URL::asset('utama/img/bg-img/gambar'.$i.'.jpg')}}" alt="" style="width: 100%;height: 35%">                    
                  </div>                  
                  <div class="product-desc text-center pt-4">
                    <a href="{{url('detail/1')}}" class="product-title">
                    @if($i == 4)
                      Strawberry
                    @elseif($i == 5)
                      Sayur Hijau
                    @elseif($i == 6)
                      Tomat
                    @elseif($i == 7)
                      Cabai
                    @endif

                    </a>
                  </div>
                </div>
              </div>                        
            @endfor
            </div>          

      </section>
      <!-- ##### Our Products Area End ##### -->

          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
<script src="{{URL::asset('utama/js/jquery.min.js')}}"></script>
@section('script')

@endsection