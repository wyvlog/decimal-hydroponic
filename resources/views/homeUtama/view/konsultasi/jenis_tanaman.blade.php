@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url({{url('utama/img/bg-img/18.jpg')}});">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Konsultasi</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Konsultasi</li>
          <li class="breadcrumb-item active" aria-current="page">Jensi Tanaman</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="farming-practice-area section-padding-100-50">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- Section Heading -->
          <div class="section-heading text-center">
            <p>Kosultasi Tanaman Sesui Jenis</p>
            <h2><span>Pilih Tanaman</span> Yang Inggin Konsultasikan</h2>
            <img src="{{URL::asset('utama/img/core-img/decor2.png')}}" alt="">
          </div>
        </div>
      </div>

      <div class="row">
        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="500ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar7.jpg')}}" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="{{URL::asset('utama/img/core-img/vegetable.png')}}" alt="">
              </div>
              <span>Cabai</span>
              <a href="{{route('jenisTanamanCabai')}}"><h4>Konsultasi Tanaman Cabai</h4></a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="500ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar6.jpg')}}" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="{{URL::asset('utama/img/core-img/vegetable.png')}}" alt="">
              </div>
              <span>Tomat</span>
              <a href="{{route('jenisTanamanCabai')}}"><h4>Konsultasi Tanaman Tomat</h4></a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="500ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar5.jpg')}}" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="{{URL::asset('utama/img/core-img/vegetable.png')}}" alt="">
              </div>
              <span>Sawi</span>
              <h4>Konsultasi Tanaman Sawi</h4>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="500ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="{{URL::asset('utama/img/bg-img/gambar4.jpg')}}" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="{{URL::asset('utama/img/core-img/vegetable.png')}}" alt="">
              </div>
              <span>Stroberry</span>
              <h4>Konsultasi Tanaman Stroberry</h4>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
@section('script')

@endsection
