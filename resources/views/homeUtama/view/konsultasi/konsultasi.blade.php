@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Konsultasi</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Konsultasi</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Thumbnail -->
        <div class="col-12 col-lg-6">
          <div class="about-us-thumbnail mb-100">
            <img src="{{URL::asset('utama/img/bg-img/24.jpg')}}" alt="">
          </div>
        </div>

        <!-- About Us Content -->
        <div class="col-12 col-lg-6">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Konsultasi Penyakit Tanaman</span> Hydroponic</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            <p>Konsultasi Tanaman akan meberikan solusi jika terjadi sesuatu yang tidak terjadi sebagai mestinya pada tanaman yang ditanam, bagi petani atau orang awam yang ingin belajr bertani hydroponic</p>
            <a href="{{route('jenisTanaman')}}" class="btn famie-btn mt-30">Mulai</a>
          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
@section('script')

@endsection
