@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Konsultasi</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Konsultasi</li>
          <li class="breadcrumb-item active" aria-current="page">Cabai</li>
          <li class="breadcrumb-item active" aria-current="page">Hasil</li>
          <li class="breadcrumb-item active" aria-current="page">jamur Fusarium oxysporum</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Thumbnail -->
        <div class="col-12 col-lg-6">
          <div class="about-us-thumbnail mb-100">
            <img src="{{URL::asset('utama/img/bg-img/gambar6.jpg')}}" alt="">
          </div>
        </div>

        <!-- About Us Content -->
        <div class="col-12 col-lg-6">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Tanaman Tomat Terkena</span> Jamur Fusarium Oxysporum</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            <p>Penyakit layu fusarium disebabkan oleh serangan jamur Fusarium oxysporum. Jamur ini awalnya menyerang dari akar kemudian berkembang ke lewat jaringan pembuluh. Tanaman tomat yang terkena penyakit ini akan berubah menjadi layu dan mati.

Jaringan pembuluh yang terserang berwarna coklat dan menghambat aliran air dari akar ke daun. Sehingga daun dan batang atas menjadi layu.

Pada malam hari tanaman masih terlihat segar, begitu ada sinar matahari dan terjadi penguapan tanaman dengan cepat menjadi layu. Pada sore harinya, bisa kembali menjadi segar dan keesokan harinya akan layu kembali hingga pada akhirnya mati.

Untuk menghindari serangan penyakit ini gunakan benih yang resisten. Penggunaan mulsa plastik juga bisa menekan perkembangan jamur dalam tanah. Hindari budidaya tanaman tomat pada bekas lahan yang pernah terserang jamur ini. Berikan jeda yang cukup lama hingga bisa kembali ditanami tomat.</p>
            <a href="{{route('jenisTanaman')}}" class="btn famie-btn mt-30">Konsultasi Lagi</a>
          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
@section('script')

@endsection
