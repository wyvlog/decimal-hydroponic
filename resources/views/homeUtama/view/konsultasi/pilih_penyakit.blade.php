@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Konsultasi</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Konsultasi</li>
          <li class="breadcrumb-item active" aria-current="page">Cabai</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Content -->
        <div class="col-12 col-lg-12">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Konsultasi Penyakit Tanaman Tomat</span> Hydroponic</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            <form>
              <div class="checkbox">
                <label><input type="checkbox" value="">Pada malam hari tanaman masih terlihat segar, begitu ada sinar matahari dan terjadi penguapan tanaman dengan cepat menjadi layu</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Tanaman Tomat Layu</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Daun terdapat bercak coklat hingga hitam</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Terlihat bercak kecil berwarna coklat pada buah Tomat</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Pada pangkal buah dekat tangkai terdapat bercak ungu</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Bercak kecil berair, membulat dan cekung pada buah</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Pada buah pada mulanya terlihat bercak berair dan berubah menjadi bercak bergabus</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Daun terlihat keriting dan mengering</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Batang terlihat kering memanjang berwarna keabu-abuan</label>
              </div>
            </form>
            <a href="{{route('hasil')}}" class="btn famie-btn mt-30">Cek Hasil Konsultasi</a>
            
          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
@section('script')

@endsection
