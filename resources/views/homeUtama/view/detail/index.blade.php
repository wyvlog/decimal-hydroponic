@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/gambar7.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Tanaman Cabai</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Tanaman Cabai</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Content -->
        <div class="col-12 col-lg-12">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Tanaman</span> Hydroponic</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            
            <center><h2>Cabai</h2></center>
            <div class="row">
              <table class="table">
                <tr>
                  <th>Nama</th>
                  <td>Tanaman Cabai</td>
                </tr>
                <tr>
                  <th>Jenis Tanaman</th>
                  <td>Sayuran Buah</td>
                </tr>
                <tr>
                  <th>Harga per bibit</th>
                  <td>Rp. 2.500</td>
                </tr>
                <tr>
                  <th>Lama waktu penanaman</th>
                  <td>80 s/d 90 hari</td>
                </tr>
                <tr>
                  <th>pH</th>
                  <td>6.0 - 6.5</td>
                </tr>
                <tr>
                  <th>PPM</th>
                  <td>1260 - 1540</td>
                </tr>
                <tr>
                  <th>Tahap Penanaman</th>
                  <td>
                    • Tahap 1 :
                    Memililih varietas unggul cabe yang ada di Indonesia. Terdapat beberapa jenis mulai dari hybrida sampai lokal. Saran saya sebaiknya gunakan cabe hybrida, karena selain bagus buahnya, jenis cabe ini juga mempunyai daya tahan yang cukup terhadap hama. Selanjutnya semailah bibit yang sudah dipilih kedalam wadah plybag atau pot kecil. Anda juga bisa menggunakan bekas gelas minuman mineral.
                    </td>
                  </tr>
                  <tr>
                    <th></th>
                    <td width="60%">
                    • Tahap 2 :
                    Pemindahan cabe setelah disemai kedalam media tanaman hidroponik, berlangsung sekitar 5-7 hari saat menyemai. Baru, setelah itu bisa dipindahkan. Jangan lupa juga untuk menambahakan larutan nutrisi pada media.
                    </td>
                  </tr>
                  <tr>
                    <th></th>
                    <td>
                    • Tahap 3 :
                    Perawatan dilakukan seperti biasa, yatu dengan mengontrol volume larutan nutrisi. Jika sudah sedikit lakukan penambahan kembali. Selain itu penyinaran matahari juga perlu diperhatikan supaya cabe dapat tumbuh dengan baik.  
                    </td>
                  </tr>
                  <tr>
                    <th></th>
                    <td>
                    • Tahap 4 :
                    Cabe termasuk kedalam tanaman hidroponik yang mempunyai masa panen 80 sampai 90 hari. Ciri yang bisa ditemui pada cabe siap panen adalah mulai muncul warna merah dan garis hijau yang sudah memudar.
                    </td>
                  </tr>                    
              </table>
            </div>          

      </section>
      <!-- ##### Our Products Area End ##### -->

          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
<script src="{{URL::asset('utama/js/jquery.min.js')}}"></script>
@section('script')

@endsection