@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/17.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Rekomendasi</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Rekomendasi</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Content -->
        <div class="col-12 col-lg-12">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Rekomendasi Tanaman</span> Hydroponic</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            
            <label>Budget</label>            
            <input type="text" name="" placeholder="Masukkan budget anda" class="form-control">                 

            <label>Target Jumlah Tanaman</label>            
            <input type="number" name="" placeholder="Masukkan target jumlah tanaman" class="form-control">

            <label>Jenis Tanaman</label>
            <select class="form-control">
              <option disabled hidden selected>Pilih jenis tanaman</option>
              <option>Semua Jenis</option>
              <option>Sayuran Daun</option>
              <option>Sayuran Buah</option>
              <option>Buah</option>
              <option>Bunga</option>
              <option>Herbal</option>
              <option>Umbi</option>
            </select>

            <label>Target Waktu Penanaman</label>
            <select class="form-control">
              <option disabled hidden selected>Pilih Target Waktu</option>
              <option>Kurang 20 Hari</option>
              <option>20 s/d 30 Hari</option>
              <option>30 s/d 40 Hari</option>
              <option>40 s/d 50 Hari</option>
              <option>50 s/d 60 Hari</option>
              <option>Lebih dari 60 Hari</option>
              <option>Semua Rentang</option>
            </select>

            <center><button class="btn famie-btn" id="rekomendasi">Lihat Rekomendasi</button></center>
            <br>

            <center><h2 id="title-hasil" style="display: none;">Hasil Rekomendasi</h2></center>
            <div class="row">
            @for($i=4;$i<=7;$i++)
              <!-- Single Product Area -->
              <div class="col-12 col-sm-6 col-lg-3" id="result_{{$i}}" style="display: none;">
                <div class="single-product-area mb-50 fadeInUp">
                  <!-- Product Thumbnail -->
                  <div class="product-thumbnail">
                    <img src="{{URL::asset('utama/img/bg-img/gambar'.$i.'.jpg')}}" alt="" style="width: 100%;height: 35%">
                    <!-- Product Tags -->
                    <!-- Product Meta Data -->
                    <div class="product-meta-data">
                      <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="icon_heart_alt"></i></a>
                  
                    </div>
                  </div>
                  <!-- Product Description -->
                  <div class="product-desc text-center pt-4">
                    <a href="#" class="product-title">
                    @if($i == 4)
                      Strawberry
                    @elseif($i == 5)
                      Sayur Hijau
                    @elseif($i == 6)
                      Tomat
                    @elseif($i == 7)
                      Cabai
                    @endif

                    </a>
                  </div>
                </div>
              </div>                        
            @endfor
            </div>

      </section>
      <!-- ##### Our Products Area End ##### -->

          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
<script src="{{URL::asset('utama/js/jquery.min.js')}}"></script>
<script>
  $(document).ready(function(){

    $('#rekomendasi').click(function(){      
      $('#title-hasil').fadeIn();      
      for(var i=4;i<=7;i++){
        
          pop(i);
        
      }
    });

    function pop(i){
      setTimeout(function(){
        $('#result_'+i).fadeIn();
      }, 250*(i-4));
    }

  });
</script>
@section('script')

@endsection