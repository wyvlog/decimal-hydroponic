@extends('homeUtama.layout.master')
@section('breadcrump')
      <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('utama/img/bg-img/27.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Video</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Video</li>
        </ol>
      </nav>
    </div>
  </div>
@stop
@section('content')
    <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Content -->
        <div class="col-12 col-lg-12">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <h2><span>Video Tanaman</span> Hydroponic</h2>
              <img src="{{URL::asset('utama/img/core-img/decor.png')}}" alt="">
            </div>
            
            <div class="row">          
              @for($i=1;$i<=4;$i++)
              <!-- Single Product Area -->
              <div class="col-12 col-sm-6 col-lg-6">
                <div class="single-product-area mb-50 fadeInUp wow fadeInUp" data-wow-delay="{{250*$i}}ms">
                  <!-- Product Thumbnail -->                  
                  <div class="famie-video-plays">
                    <img src="{{URL::asset('utama/img/video-img/'.$i.'.jpg')}}" alt="">
                    <!-- Play Icon -->
                    <a href="https://www.youtube.com/watch?v=9l-ti-tT9xw" class="play-icon"><i class="fa fa-play"></i></a>
                  </div>        
                  <!-- Product Description -->
                  <div class="product-desc text-center pt-4">

                    <a href="https://www.youtube.com/watch?v=9l-ti-tT9xw" class="product-title">
                      @if($i == 1)
                      Tutorial Hidroponik untuk Pemula
                      @elseif($i == 2)
                      Cara Menanam Bawang Merah Hidroponik
                      @elseif($i == 3)
                      Sistem Hidroponik Sederhana, Hasil Luar Biasa
                      @elseif($i == 4)
                      Hidroponik RAKIT APUNG Sederhana
                      @endif
                    </a>                               

                  </div>
                </div>
              </div> 
            @endfor
            </div>            

      </section>
      <!-- ##### Our Products Area End ##### -->

          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
<script src="{{URL::asset('utama/js/jquery.min.js')}}"></script>
@section('script')

@endsection