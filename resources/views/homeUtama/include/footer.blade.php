<footer class="footer-area">
    <!-- Main Footer Area -->
    <div class="main-footer bg-img bg-overlay section-padding-80-0" style="background-image: url({{url('utama/img/bg-img/3.jpg')}});">
      <div class="container">
        <div class="row">

          <!-- Single Footer Widget Area -->
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="footer-widget mb-80">
              <a href="#" class="foo-logo d-block mb-30"><img src="{{URL::asset('utama/img/core-img/logoS.png')}}"  style="width:80%" alt=""></a>
              <p>Smart Ponik Education</p>
              <div class="contact-info">
                <p><i class="fa fa-map-pin" aria-hidden="true"></i><span>Denpasar, Bali</span></p>
                <p><i class="fa fa-envelope" aria-hidden="true"></i><span>semartpon@gmail.com</span></p>
                <p><i class="fa fa-phone" aria-hidden="true"></i><span>+629602641324</span></p>
              </div>
            </div>
          </div>

          <!-- Single Footer Widget Area -->
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="footer-widget mb-80">
              <h5 class="widget-title">Fitur Utama</h5>
              <!-- Footer Widget Nav -->
              <nav class="footer-widget-nav">
                <ul>
                  <li><a href="#">Rekomendasi</a></li>
                  <li><a href="#">Keperlaun</a></li>
                  <li><a href="#">Cara Penanaman dan penjadwalan</a></li>
                  <li><a href="#">Video</a></li>
                  <li><a href="#">Forum Diskusi</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <!-- Single Footer Widget Area -->
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="footer-widget mb-80">
              <h5 class="widget-title">NEWS</h5>

              <!-- Single Recent News Start -->
              <div class="single-recent-blog d-flex align-items-center">
                <div class="post-thumbnail">
                  <img src="{{URL::asset('utama/img/bg-img/gambar1.jpg')}}" alt="">
                </div>
                <div class="post-content">
                  <a href="#" class="post-title"></a>
                  <div class="post-date">11 Maret 2019</div>
                </div>
              </div>

              <!-- Single Recent News Start -->
              <div class="single-recent-blog d-flex align-items-center">
                <div class="post-thumbnail">
                  <img src="{{URL::asset('utama/img/bg-img/gambar2.jpg')}}" alt="">
                </div>
                <div class="post-content">
                  <a href="#" class="post-title"></a>
                  <div class="post-date">11 Maret 2019</div>
                </div>
              </div>

            </div>
          </div>

          <!-- Single Footer Widget Area -->
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="footer-widget mb-80">
              <h5 class="widget-title">Sosial Media</h5>
              <!-- Footer Social Info -->
              <div class="footer-social-info">
                <a href="#">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                  <span>Facebook</span>
                </a>
                <a href="#">
                  <i class="fa fa-twitter" aria-hidden="true"></i>
                  <span>Twitter</span>
                </a>
                <a href="#">
                  <i class="fa fa-pinterest" aria-hidden="true"></i>
                  <span>Pinterest</span>
                </a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </footer>