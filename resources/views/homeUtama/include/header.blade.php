  <header class="header-area">
    <!-- Top Header Area -->
    <div class="top-header-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="top-header-content d-flex align-items-center justify-content-between">
              <!-- Top Header Content -->
              <div class="top-header-meta">
                <p>Selamat Datang Pada <span>SMARTPONE</span>, Kami Harap Senang Mengunakan Aplikasi ini</p>
              </div>
              <!-- Top Header Content -->
              <div class="top-header-meta text-right">
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="smartpone@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Email: smartpone@gmail.com</span></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="+62 960 264 1324"><i class="fa fa-phone" aria-hidden="true"></i> <span>Call Us: +62 960 264 1324</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Navbar Area -->
    <div class="famie-main-menu">
      <div class="classy-nav-container breakpoint-off">
        <div class="container">
          <!-- Menu -->
          <nav class="classy-navbar justify-content-between" id="famieNav">
            <!-- Nav Brand -->
            <a href="index.html" class="nav-brand"><img style="width: 80%;" src="{{URL::asset('utama/img/core-img/logoS.png')}}" alt=""></a>
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
              <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu">
              <!-- Close Button -->
              <div class="classycloseIcon">
                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
              </div>
              <!-- Navbar Start -->
              <div class="classynav">
                <!-- Search Icon -->
                <div id="searchIcon">
                  <i class="icon_search" aria-hidden="true"></i>
                </div>
                <!-- Cart Icon -->
                <ul>
                  <li class="active"><a href="{{route('/')}}">Home</a></li>
                  <li><a href="about.html">Tentang</a></li>
                  <li><a href="#">Fitur</a>
                    <ul class="dropdown">
                      <li><a href="index.html">Rekomen Tanaman</a></li>
                      <li><a href="about.html">Konsultasi </a></li>
                      <li><a href="farming-practice.html">Penjadwalan</a></li>
                      <li><a href="shop.html">Video Tanaman</a>
                        <ul class="dropdown">
                          <li><a href="our-product.html">Our Products</a></li>
                          <li><a href="shop.html">Shop</a></li>
                        </ul>
                      </li>
                      <li><a href="news.html">News</a>
                        <ul class="dropdown">
                          <li><a href="news.html">News</a></li>
                          <li><a href="news-details.html">News Details</a></li>
                        </ul>
                      </li>
                      <li><a href="contact.html">Forum Diskusi</a></li>
                      
                    </ul>
                  </li>
                  <li><a href="#"></a></li>
                  <li><a href="#"></a></li>
                  <li><a href="{{route('loginn')}}">Login</a></li>
                </ul>
                
              </div>
              <!-- Navbar End -->
            </div>
          </nav>

          <!-- Search Form -->
          <div class="search-form">
            <form action="#" method="get">
              <input type="search" name="search" id="search" placeholder="Type keywords &amp; press enter...">
              <button type="submit" class="d-none"></button>
            </form>
            <!-- Close Icon -->
            <div class="closeIcon"><i class="fa fa-times" aria-hidden="true"></i></div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- ##### Header Area End ##### -->