@extends('admin.layout.master')
@section('breadcrump')
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard Admin</li>
      </ol>
@stop
@section('content')
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <!-- /.box-header -->
             <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Dashboard Admin</h3>
              </div>
              <div class="box-body">
                Selamat Anda Berhasil Login Sebagai Admin
              </div>
              
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
@endsection
@section('script')

@endsection
