  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{URL::asset('admin/dist/img/logo.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin Inventaris</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="active">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Upload Inventaris</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{Route('inventaris.meja')}}"><i class="fa fa-circle-o"></i>Meja</a></li>
            <li><a href="{{Route('inventaris.kursi')}}"><i class="fa fa-circle-o"></i>Kursi</a></li>
            <li><a href="{{Route('/')}}"><i class="fa fa-circle-o"></i>AC</a></li>
            <li><a href="{{Route('/')}}"><i class="fa fa-circle-o"></i>Proyektor</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Buat Kategori</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{Route('kategori.meja')}}"><i class="fa fa-circle-o"></i>Kategori Meja</a></li>
            <li><a href="{{Route('kategori.kursi')}}"><i class="fa fa-circle-o"></i>Kategori Kursi</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Master Data Inventaris</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('/')}}"><i class="fa fa-circle-o"></i>View Inventaris</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('/')}}"><i class="fa fa-circle-o"></i>Report Inventaris</a></li>
          </ul>
        </li>
       
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>