@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/auth/index.css')}}">
<link rel="stylesheet" href="{{asset('css/auth/login.css')}}">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-6">
            <div class="card">

                <div class="card-body">
                    <h4>Login Inventaris</h4>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Masukkan Email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Masukkan Password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary col-md-12 btn-login">
                                    {{ __('Login') }}
                                </button>                                
                            </div>
                        </div>
                    </form>
                    <div class="border-outline"></div>
                    <p class="has-account">Belum Punya Akun? <a href="{{url('register')}}">Daftar Sekarang</a></p>
                    <p class="has-account"><a href="{{url('/')}}">Lupa Kata Sandi</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
