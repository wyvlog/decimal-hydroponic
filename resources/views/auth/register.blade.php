@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/auth/index.css')}}">
<link rel="stylesheet" href="{{asset('css/auth/register.css')}}">
<div class="container">
    <div style="text-align: center; color: white;">
    </div>
    <div class="row justify-content-center">

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4>Register Inventaris</h4>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                         <div class="form-group row">
                            
                            <!-- <div class="wrap-label-input col-md-12">
                                <label for="name" class="col-form-label">{{ __('NIK') }}</label>
                            </div> -->
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{ old('id') }}" required autofocus placeholder="Masukkan NIK">

                                @if ($errors->has('id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input  type="hidden" name="jabatan" id="jabatan" value="1">

                        <div class="form-group row">
                            <!-- <div class="wrap-label-input col-md-12">
                                <label for="name" class="col-form-label">{{ __('Name') }}</label>
                            </div> -->

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Masukkan Nama Lengkap">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- <div class='wrap-label-input col-md-12'>
                                <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            </div> -->

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Masukkan Alamat Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- <div class="wrap-label-input col-md-12">
                                <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                            </div> -->

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Masukkan Kata Sandi">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Konfirmasi Kata Sandi">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 btn-register-wrapper">
                                <button type="submit" class="btn btn-primary btn-register">
                                    {{ __('REGISTER') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="border-outline"></div>
                    <p class="has-account">Sudah Punya Akun ? <a href="{{url('login')}}">Masuk Sekarang</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
