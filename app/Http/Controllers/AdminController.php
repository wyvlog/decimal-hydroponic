<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Response;
use DB;
use Validator;
use App\User;
use PDF;
use Carbon\Carbon;
use DataTables;

class AdminController extends Controller
{
    public function kegiatan()
    {
    	$data = seminar::all();
    	return view('admin.dashboard.kegiatan.list_kegiatan',$data);
    }
    public function presensi()
    {
    	$data['seminar'] = seminar::orderByRaw('id_seminar DESC')
    	->get();
    	//dd($data);
    	return view('admin.dashboard.report.presensi',$data);
    }
    public function absen($id)
    {
    	$data['peserta'] = daftar::select(DB::raw('td_daftar.*,users.id as nim,users.name as name, users.no_tlp as no_tlp,users.email as email'))
    	->join('users','users.id','=','td_daftar.id_user') 
    	->where('id_seminar',$id)
    	->get();

    	$data ['kegiatan'] = seminar::where('id_seminar',$id)
    	->first();

    	$data['jumlahpeserta'] = daftar::where('id_seminar',$id)
    	->count();
    	// $data['peserta'] = daftar::select(DB::raw('td_daftar.*'))->get();
    	// dd($data);
    	$pdf =  PDF::loadView('admin.dashboard.report.absen_pdf', $data);
    	return $pdf->stream();
    }
    public function Totalkegiatan()
    {
        $data['dataSeminar'] = seminar::all();
    	return view('admin.dashboard.report.list_jumlah_kegiatan',$data);
    }
    public function pesertaKegiatan()
    {
    	$data['dataSeminar'] = seminar::all();
    	return view('admin.dashboard.report.list_peserta_kegiatan',$data);
    }
    public function getData(Request $request)
    {   
        $query = User::select(
                    'users.name',
                    'users.email',
                    'users.alamat',
                    'users.id',
                    'users.no_tlp',
                    'td_daftar.id_daftar',
                    'td_daftar.id_seminar',
                    'tb_kehadiran.id_kehadiran'
                 )
                 ->join('td_daftar','td_daftar.id_user','users.id')
                 ->join('tb_seminar','tb_seminar.id_seminar','td_daftar.id_seminar')
                 ->leftJoin('tb_kehadiran','tb_kehadiran.id_daftar','td_daftar.id_daftar')
                 ->where('tb_seminar.id_seminar',$request->id_seminar);
        //dd($query);
        return Datatables::of($query)->make(true);
    }
    public function cetak(Request $request)
    {
    	$id = $request->nama_kegiatan;
    	$data['peserta'] = daftar::select(DB::raw('td_daftar.*,users.id as nim,users.name as name, users.no_tlp as no_tlp,users.email as email'))
    	->join('users','users.id','=','td_daftar.id_user') 
    	->where('id_seminar',$id)
    	->get();

    	$data ['kegiatan'] = seminar::where('id_seminar',$id)
    	->first();

    	$data['jumlahpeserta'] = daftar::where('id_seminar',$id)
    	->count();
    	// $data['peserta'] = daftar::select(DB::raw('td_daftar.*'))->get();
    	// dd($data);
    	$pdf =  PDF::loadView('admin.dashboard.report.kegiatan_peserta_pdf', $data);
    	return $pdf->stream();
    }

    public function getDataKegiatan(Request $request)
    { 
        $total= daftar::select('td_daftar.id_seminar', \DB::raw('count(id_seminar) as total'))
        ->groupBy('id_seminar');


        $query = seminar::leftjoinSub($total,'total',function($join){
            $join->on('tb_seminar.id_seminar','=','total.id_seminar')
            ->select('total');
        })
        ->select(DB::raw('tb_seminar.*,total'))
        ->whereYear('tb_seminar.created_at',$request->tahun)
        ->orderBy('tb_seminar.id_seminar', 'DESC');


        // $query = seminar::select(
        //             'tb_seminar.*'
        //          )
        //         // ->join('tb_tglseminar','tb_tglseminar.id_seminar','=','tb_seminar.id_seminar')
        //         ->whereYear('tb_seminar.created_at',$request->tahun)
        //         ->orderBy('tb_seminar.id_seminar', 'DESC');

        return Datatables::of($query)->make(true);
    }
    public function cetakKegiatan(Request $request)
    {
        $id = $request->tahun;

        $total= daftar::select('id_seminar', \DB::raw('count(id_seminar) as total'))
        ->groupBy('id_seminar');


        $data['sub'] = seminar::leftjoinSub($total,'total',function($join){
            $join->on('tb_seminar.id_seminar','=','total.id_seminar');
        })
        ->select(DB::raw('tb_seminar.*,total'))
        ->whereYear('tb_seminar.created_at',$id)
        ->orderBy('tb_seminar.id_seminar', 'DESC')
        ->get();

        $data ['judul'] = 'Report Total Kegiatan Selama Tahun '.$id;
        $data['totalpeserta'] = seminar::join('td_daftar','td_daftar.id_seminar','=','tb_seminar.id_seminar')
        ->whereYear('tb_seminar.created_at',$id)
        ->count();
        // $data['peserta'] = daftar::select(DB::raw('td_daftar.*'))->get();
        // dd($data);
        $pdf =  PDF::loadView('admin.dashboard.report.kegiatan_pdf', $data);
        return $pdf->stream();
    }
    public function query()
    {
    // $total= daftar::select('id_seminar', \DB::raw('count(id_seminar) as total'))
    //     ->groupBy('id_seminar');


    //     $data['sub'] = seminar::leftjoinSub($total,'total',function($join){
    //         $join->on('tb_seminar.id_seminar','=','total.id_seminar');
    //     })
    //     ->select(DB::raw('tb_seminar.*,total'))
    //     ->whereYear('tb_seminar.created_at',2019)
    //     ->orderBy('tb_seminar.id_seminar', 'DESC')
    //     ->get();
        $terdaftar = daftar::join('tb_kehadiran','tb_kehadiran.id_daftar','=','td_daftar.id_daftar')
        ->count();
        $total = daftar::leftjoin('tb_kehadiran','tb_kehadiran.id_daftar','=','td_daftar.id_daftar')
        ->count();

        $data = $total - $terdaftar;
        dd($data);

    }
    public function getDataChart(){
        $minDate = seminar::leftJoin('tb_tglseminar','tb_tglseminar.id_seminar','tb_seminar.id_seminar')
        ->min('tb_tglseminar.tgl_seminar');

        $maxDate = seminar::leftJoin('tb_tglseminar','tb_tglseminar.id_seminar','tb_seminar.id_seminar')
        ->max('tb_tglseminar.tgl_seminar');

        $maxTahunSeminar = Carbon::parse($maxDate)->year;
        $minTahunSeminar = Carbon::parse($maxDate)->year;
        
        for($i=$minTahunSeminar; $i<=$maxTahunSeminar; $i++){
            $jumlahPeserta = seminar::leftJoin('td_daftar','td_daftar.id_seminar','tb_seminar.id_seminar')
            ->whereYear('td_daftar.created_at',$i)->count() ;
            $data['dataLineChart'][] = [
                'y' => ''.$i,
                'x' => $jumlahPeserta,
            ];
        }
        
        $currentYear = Carbon::now()->year;
        $lastTglSeminar = tanggal::select('id_seminar',DB::raw('MAX(tgl_seminar) as tgl_seminar'))
                          ->whereYear('tb_tglseminar.tgl_seminar', $currentYear)
                          ->groupBy('id_seminar');
        
        $listSeminar = seminar::joinSub($lastTglSeminar, 'last_tgl_seminar', function($join){
            $join->on('tb_seminar.id_seminar','=','last_tgl_seminar.id_seminar');
        })->get();
        
        for($i=0;$i<sizeOf($listSeminar);$i++){
            $jumlahPeserta = seminar::join('td_daftar','td_daftar.id_seminar','tb_seminar.id_seminar')
            ->where('tb_seminar.id_seminar',$listSeminar[$i]->id_seminar)->count() ;
            $data['dataBarChart'][] = [
                'y' => ''.$jumlahPeserta,
                'x' => $i,
                'z' => $listSeminar[$i]->nama_kegiatan
            ];
        }
        $data['dataChart'] = [
            'line' => 'Data Tahun '.$minTahunSeminar.' - '.$maxTahunSeminar,
            'bar' => 'Data Tahun '.$currentYear
        ];
        return $data;
    }

}
