<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AuthController extends Controller
{
      public function __construct(Request $request)
    {
      $this->middleware('auth');
    }

    public function index(Request $request){
      $level = Auth::user()->jabatan;
      //dd($level);

      switch ($level) {
        case "1":
            return $this->dashboardLevel1(); //Admin
            break;
        case "2":            
            return $this->dashboardLevel2(); //User
            break;
        default:
            echo "error";
      }
    }

    protected function dashBoardLevel1()
    {
      return view('admin.dashboard.homeAdmin');
    }
    protected function dashBoardLevel2()
    {
      return view('homeUtama.view.index');
    }
}
