<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        return view('homeUtama.view.index');
    }
    public function login()
    {
        return view('login.login');
    }
    public function konsultasi()
    {
    	return view('homeUtama.view.konsultasi.konsultasi');
    }
    public function konsultasiTanaman()
    {
    	return view('homeUtama.view.konsultasi.jenis_tanaman');
    }
    public function konsultasiTanamanCabai()
    {
        return view('homeUtama.view.konsultasi.pilih_penyakit');
    }
    public function hasil()
    {
        return view('homeUtama.view.konsultasi.hasil');
    }
}
