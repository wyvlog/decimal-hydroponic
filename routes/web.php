<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});

Route::group(['middleware' => ['web']], function()    
{
	Route::get('/', 'HomeController@index')->name('/');
	Route::get('loginn', 'HomeController@login')->name('loginn');
	Route::get('konsultasi', 'HomeController@konsultasi')->name('konsultasi');
	Route::get('konsultasi/jenisTanaman', 'HomeController@konsultasiTanaman')->name('jenisTanaman');
	Route::get('konsultasi/jenisTanaman/cabai', 'HomeController@konsultasiTanamanCabai')->name('jenisTanamanCabai');
	Route::get('konsultasi/jenisTanaman/cabai/hasil', 'HomeController@hasil')->name('hasil');

	Route::get('rekomendasi','RekomendasiController@index');
	Route::get('video','VideoController@index');
	Route::get('penjadwalan','PenjadwalanController@index');
	Route::get('detail/{id}','DetailController@index');
	// Route::get('/home', 'AuthController@index')->name('home');
});



//Admin
Route::group(['middleware' => ['web','auth','jabatan:1']], function()
{
	//Kategori
	//Route::get('/kategori/meja', array('as'=>'kategori.meja', 'uses'=>'InventarisController@kategoriMeja'));
});

//User
Route::group(['middleware' => ['web','auth','jabatan:2']], function()
{
	//Route::post('/cetak', array('as'=>'cetak', 'uses'=>'AdminController@cetak'));
});
